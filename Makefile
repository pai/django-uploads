DOCKER=docker
DPATH=${CURDIR}

IMAGE_NAME=django-uploads
VERSION=$(shell python3 -c 'import backend;print(backend.__version__)')


export DPATH
.PHONY:	build-docker push-docker

build-docker:
	$(DOCKER) build . --network host -f ./compose/production/django/Dockerfile -t quay.io/paiuolo/${IMAGE_NAME} -t quay.io/paiuolo/${IMAGE_NAME}:${VERSION}

push-docker:
	$(DOCKER) push quay.io/paiuolo/${IMAGE_NAME}:${VERSION}
	$(DOCKER) push quay.io/paiuolo/${IMAGE_NAME}:latest
