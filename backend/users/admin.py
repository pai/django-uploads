from django.contrib import admin
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from django_sso_app.core.apps.users.admin import UserAdmin as DjangoSsoAppUserAdmin

User = get_user_model()


class UserAdmin(DjangoSsoAppUserAdmin):
    fieldsets = DjangoSsoAppUserAdmin.fieldsets + (
        (None, {
            'fields': ('ftp_password', 'ftp_quota'),
        }),
    )


admin.site.register(User, UserAdmin)
