import logging

import socket

from django.conf import settings

from django.contrib.sites.models import Site

from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django_sso_app.core.apps.users.serializers import UserSerializer

from apps.permissions import is_staff
from apps.utils import local_space_available
from apps.settings import CURRENT_DIR, USERS_FOLDER, MAX_FILE_SIZE_MB, MINIMUM_FREE_SPACE_MB
from apps.uploads.models import UploadType
from apps.uploads.serializers import UploadTypeSerializer

from backend import dist_name, __version__

logger = logging.getLogger('backend.api')


class StatsView(APIView):
    """
    Return instance stats.
    """

    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        try:
            health_status = 'red'
            free_space_mb = int(local_space_available(CURRENT_DIR) / (1024 * 1024))

            context = {
                'request': request
            }

            logger.info(
                'Free space (MB) in folder {}: {}. Minimum free space: {}'.format(USERS_FOLDER,
                                                                                  free_space_mb,
                                                                                  MINIMUM_FREE_SPACE_MB))

            if free_space_mb > MINIMUM_FREE_SPACE_MB:
                health_status = 'green'
            else:
                health_status = 'yellow'

            sites = []
            for s in Site.objects.all().order_by('id'):
                sites.append(
                    {
                        'url': settings.ACCOUNT_DEFAULT_HTTP_PROTOCOL + '://'
                               + s.domain
                    })

            upload_types = UploadType.get_user_upload_types(request.user)

            upload_types_serializer = UploadTypeSerializer(upload_types,
                                                           context={
                                                               'request': request},
                                                           many=True)

            data = {
                'app_name': dist_name,
                'deployment_environment': settings.DEPLOYMENT_ENV,
                'revision': settings.REPOSITORY_REV,
                'version': __version__,

                'user': UserSerializer(request.user, context=context).data if request.user.is_authenticated else None,

                'status': health_status,
                'instances': sites,
                'upload_types': upload_types_serializer.data,
                'max_file_size_mb': MAX_FILE_SIZE_MB,

                'host': socket.gethostname()
            }

            if is_staff(request.user):
                data['free_space_mb'] = free_space_mb
                data['meta'] = str(request.META.items())

            return Response(data, status.HTTP_200_OK)

        except Exception as e:
            logger.exception('Error getting stats')
            print('StatsView Exception', e)
            return Response('Error getting stats', status.HTTP_500_INTERNAL_SERVER_ERROR)
