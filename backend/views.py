import logging

from urllib.parse import urlsplit

from django.utils import translation
from django.shortcuts import redirect
from django.conf import settings

logger = logging.getLogger('backend')


def set_language_from_url(request, user_language):
    prev_lang = request.session.get(translation.LANGUAGE_SESSION_KEY, request.LANGUAGE_CODE)

    translation.activate(user_language)
    request.session[translation.LANGUAGE_SESSION_KEY] = user_language

    url = request.META.get('HTTP_REFERER', '/')
    parsed = urlsplit(url)

    _url = parsed.path
    if len(_url) > 1:
        if settings.I18N_PATH_ENABLED:
            _url = '/{}/'.format(user_language) + _url.lstrip('/{}/'.format(prev_lang))
    else:
        _url = '/'

    return redirect(_url)
