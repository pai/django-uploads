#!/bin/bash

set -e

# Dev IP
# echo '192.168.56.104 ${PROFTPD_MASQUERADE_ADDRESS}' >> /etc/hosts
# dynamic PROFTPD_MASQUERADE_ADDRESS
if [[ -z ${PROFTPD_MASQUERADE_ADDRESS} ]]; then
export PROFTPD_MASQUERADE_ADDRESS=$(curl --connect-timeout 30 api.ipify.org)
fi


PROFTPD_CONF_FILE="/etc/proftpd/ftpd.passwd"
PROFTPD_QUOTA_FILE_1="/etc/proftpd/quota/ftpquota.limittab"
PROFTPD_QUOTA_FILE_2="/etc/proftpd/quota/ftpquota.tallytab"

# in case docker mounted this is a directory
# this fails as docker mounted dirs can't be delted
#if [ -d "$PROFTPD_CONF_FILE" ]; then rmdir /etc/proftpd/ftpd.passwd ; fi
# in case the file doesn't exist
#touch $PROFTPD_CONF_FILE

chown root $PROFTPD_CONF_FILE
chmod 660  $PROFTPD_CONF_FILE

if [ ! -f $PROFTPD_QUOTA_FILE_1 ]; then
	echo "File $PROFTPD_QUOTA_FILE_1 not found!" && ftpquota --create-table --type=limit --table-path $PROFTPD_QUOTA_FILE_1
fi
chown root $PROFTPD_QUOTA_FILE_1
chmod 660  $PROFTPD_QUOTA_FILE_1

if [ ! -f $PROFTPD_QUOTA_FILE_2 ]; then
	echo "File $PROFTPD_QUOTA_FILE_2 not found!" && ftpquota --create-table --type=tally --table-path $PROFTPD_QUOTA_FILE_2
fi
chown root $PROFTPD_QUOTA_FILE_2
chmod 660  $PROFTPD_QUOTA_FILE_2

exec proftpd -nd10
# 2>&1 >& /path/to/debug/file
