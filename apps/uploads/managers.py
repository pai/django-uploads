from django.db import models

from ..managers import UuidPKManager


class UploadManager(UuidPKManager):
    def get_queryset(self):
        return super(UploadManager, self).get_queryset().order_by('-created_at')  # .filter(is_active=True)


class FileUploadManager(UuidPKManager):
    def get_queryset(self):
        return super(FileUploadManager, self).get_queryset().order_by('-created_at')  # .filter(is_active=True)


class LinkManager(UuidPKManager):
    def get_queryset(self):
        return super(LinkManager, self).get_queryset().order_by('-created_at')  # .filter(is_active=True)
