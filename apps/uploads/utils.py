import os

from ..utils import get_user_folder_path


def get_uploads_user_folder_path(user):
    return os.path.join(get_user_folder_path(user), 'file')


def create_user_folder(user):
    user_folder_path = get_uploads_user_folder_path(user)

    try:
        original_umask = os.umask(0)
        os.makedirs(user_folder_path, mode=0o777, exist_ok=True)
    finally:
        os.umask(original_umask)

    return user_folder_path


def get_filer_file_absolute_path(filer_file):
    if getattr(filer_file, 'folder', None) is not None:
        folder = filer_file.folder

        folders = [folder.name]

        while folder.parent is not None:
            folders.append(folder.parent.name)
            folder = folder.parent

        return os.path.sep.join(reversed(folders))

    else:
        # Unsorted Uploads
        return ''


def generate_filer_filename(instance, filename):
    # instance file will be uploaded to PRIVATE_ROOT/users/<user__sso_id>/<upload_type__slug>/<filename>

    file_path = get_filer_file_absolute_path(instance)

    return os.path.join(file_path, filename)
