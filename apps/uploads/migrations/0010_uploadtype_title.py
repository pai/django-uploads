# Generated by Django 2.2.17 on 2022-03-31 14:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uploads', '0009_auto_20220309_1508'),
    ]

    operations = [
        migrations.AddField(
            model_name='uploadtype',
            name='title',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Title'),
        ),
    ]
