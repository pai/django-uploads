# Generated by Django 2.2.17 on 2021-07-29 13:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uploads', '0006_auto_20210604_1029'),
    ]

    operations = [
        migrations.AddField(
            model_name='upload',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, verbose_name='Slug'),
        ),
    ]
