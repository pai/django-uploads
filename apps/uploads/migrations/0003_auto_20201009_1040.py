# Generated by Django 2.2.12 on 2020-10-09 10:40

from django.db import migrations, models
import django.db.models.deletion
import filer.fields.file


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0012_file_mime_type'),
        ('uploads', '0002_auto_20200421_2352'),
    ]

    operations = [
        migrations.AddField(
            model_name='fileupload',
            name='filer_file',
            field=filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='filer.File'),
        ),
        migrations.AddField(
            model_name='upload',
            name='file_sha1',
            field=models.CharField(blank=True, null=True, max_length=40, verbose_name='File sha1'),
        ),
    ]
