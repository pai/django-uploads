from django.apps import AppConfig


class UploadsConfig(AppConfig):
    name = 'apps.uploads'

    def ready(self):
        try:
            from . import signals  # noqa F401

        except ImportError:
            pass
