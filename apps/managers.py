from django.db import models


class SlugPKManager(models.Manager):
    use_in_migrations = True

    def get_by_natural_key(self, slug):
        return self.get(slug=slug)


class UuidPKManager(models.Manager):
    use_in_migrations = True

    def get_by_natural_key(self, uuid):
        return self.get(uuid=uuid)
