import os
import json
import shutil

from django.urls import reverse
from django.contrib.auth import get_user_model
from django.conf import settings
from django.contrib import auth

from rest_framework import status

from django_sso_app.core.tests.factories import UserTestCase
from django_sso_app.core.apps.groups.models import Group

from apps.uploads.models import UploadType
from .utils import get_user_folder_path

User = get_user_model()


class APITestFactory(UserTestCase):
    def setUp(self):
        self.base_upload_type, _created = UploadType.objects.get_or_create(name='File', slug='file', is_public=True)

        # user
        self.user_group, _created = Group.objects.get_or_create(name='people')
        self.user_password = self._get_random_pass()
        self.user = self._get_new_user(password=self.user_password)
        self.user.sso_app_profile.add_to_group(self.user_group.name)
        self.user_folder_path = get_user_folder_path(self.user)

        # staff
        self.staff_user_password = self._get_random_pass()
        self.staff_user = self._get_new_staff_user(password=self.staff_user_password)
        self.staff_user_folder_path = get_user_folder_path(self.staff_user)

        # user2
        self.user2_group, _created = Group.objects.get_or_create(name='others')
        self.user2_password = self._get_random_pass()
        self.user2 = self._get_new_user(password=self.user2_password)
        self.user2.sso_app_profile.add_to_group(self.user2_group.name)
        self.user2_folder_path = get_user_folder_path(self.user2)

        print('USER', self.user, self.user.sso_app_profile.groups.all())
        print('USER2', self.user2, self.user2.sso_app_profile.groups.all())
        print('STAFF USER', self.staff_user)

    def tearDown(self):
        shutil.rmtree(self.user_folder_path, ignore_errors=True)
        shutil.rmtree(self.staff_user_folder_path, ignore_errors=True)

    def perform_user_login(self, username=None, password=None):
        if username is None:
            username = self.user.username
        if password is None:
            password = self.user_password

        valid_login = self._get_login_object(username, password)

        response = self.client.post(
            reverse('rest_login'),
            data=json.dumps(valid_login),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user = auth.get_user(self.client)
        self.assertTrue(user.is_authenticated)

        return response

    def perform_user_logout(self):
        response = self.client.post(
            reverse('rest_logout'),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        return response
