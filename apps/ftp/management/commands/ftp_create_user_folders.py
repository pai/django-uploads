from django.core.management.base import BaseCommand

from django.contrib.auth import get_user_model

from ...utils import create_ftp_user, create_user_folder

User = get_user_model()


class Command(BaseCommand):
    help = 'Creates FTP folders for all users (non admin)'

    def handle(self, *args, **options):
        parsed_users = 0
        try:
            for user in User.objects.filter(is_superuser=False):
                create_user_folder(user)
                create_ftp_user(user)
                parsed_users += 1

        except Exception as e:
            self.stdout.write(self.style.ERROR('Error parsing ftp user: "{}"'.format(e)))

        self.stdout.write(self.style.SUCCESS('{} users ftp loaded'.format(parsed_users)))
